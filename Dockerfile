FROM registry.gitlab.com/ulrichschreiner/base/ubuntu:24.04

MAINTAINER Ulrich Schreiner <ulrich.schreiner@gmail.com>

ENV GO_VERSION=1.24.1 \
    DOCKER_VERSION=27.5.0 \
    DOCKER_BUILDX=0.20.0 \
    HELM2_VERSION=2.17.0 \
    HELM3_VERSION=3.10.2 \
    KUBECTL_VERSION=1.32.1 \
    DOCTL_VERSION=1.120.2 \
    ARGOCD_VERSION=2.4.15 \
    KUSTOMIZE_VERSION=4.5.5 \
    HTMLQ_VERSION=0.4.0


ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get -y install apt-transport-https ca-certificates curl gnupg2
RUN apt-get update && apt-get -y install \
    autoconf \
    autotools-dev \
    build-essential \
    buildah \
    bzr \
    esbuild \
    gcc \
    gettext-base \
    git \
    jq \
    libtool \
    make \
    mercurial \
    nodejs \
    npm \
    python3 \
    skopeo \
    ssh \
    tar \
    webpack \
    zip && \
    apt autoremove && \
    rm -rf /var/lib/apt/lists/*

RUN curl -SL https://storage.googleapis.com/golang/go${GO_VERSION}.linux-amd64.tar.gz | tar -C /usr/local -xz \
    && curl -SL https://dl.k8s.io/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl >/usr/bin/kubectl \
    && chmod +x /usr/bin/kubectl \
    && ln -sf /root/go/bin/* /usr/bin/ \
    && curl -SsL https://github.com/mgdm/htmlq/releases/download/v${HTMLQ_VERSION}/htmlq-x86_64-linux.tar.gz | tar -C /usr/local/bin -xz \
    && mkdir -p /root/.docker/cli-plugins \
    && curl -sSL -o /root/.docker/cli-plugins/docker-buildx https://github.com/docker/buildx/releases/download/v${DOCKER_BUILDX}/buildx-v0.20.0.linux-amd64 \
    && chmod +x /root/.docker/cli-plugins/docker-buildx \
    && curl -SL https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKER_VERSION}.tgz | tar -xzC /usr/bin --strip-components=1 docker/docker \
    && curl -SL https://github.com/digitalocean/doctl/releases/download/v${DOCTL_VERSION}/doctl-${DOCTL_VERSION}-linux-amd64.tar.gz | tar -xzC /usr/bin \
    && curl -SL https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize/v${KUSTOMIZE_VERSION}/kustomize_v${KUSTOMIZE_VERSION}_linux_amd64.tar.gz | tar -xzC /usr/bin \
    && chmod +x /usr/bin/docker \
    && chmod +x /usr/bin/doctl \
    && curl -SL https://api.github.com/repos/google/go-containerregistry/releases/latest | jq -r ".assets[] | select(.name | contains(\"Linux_x86_64\")) | .browser_download_url" | wget -i - -O - | tar  -C /usr/local/bin -xz \
    && curl -SL https://get.helm.sh/helm-v${HELM2_VERSION}-linux-amd64.tar.gz \
       | tar --strip-components 1 -xzC /usr/bin linux-amd64/helm \
    && chmod +x /usr/bin/helm && mv /usr/bin/helm /usr/bin/helm2 \
    && curl -SL https://get.helm.sh/helm-v${HELM3_VERSION}-linux-amd64.tar.gz \
       | tar --strip-components 1 -xzC /usr/bin linux-amd64/helm \
    && chmod +x /usr/bin/helm \
    && curl -SL https://github.com/argoproj/argo-cd/releases/download/v${ARGOCD_VERSION}/argocd-linux-amd64 >/usr/bin/argocd \
    && chmod +x /usr/bin/argocd \
    && npm install -g pnpm bun

ENV PATH $PATH:/usr/local/go/bin
CMD ["bash"]
